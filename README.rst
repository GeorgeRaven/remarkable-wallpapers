ReMarkable2 Wallpapers
======================

This repository is the home of any and all of my custom wallpapers for the
ReMarkable 2 and its E-Ink display.

Depending on the version of remarkable these wallapers should overwrite the
respective files that they are to replace in
`/usr/share/remarkable/*.png`. Please make sure that you back up any files you
overwrite. I cannot distributed the originals as they are not my copyright,
but please do back up the ones you find existing on your tablet.
